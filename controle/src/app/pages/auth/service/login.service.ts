import { Injectable } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
//import { User } from '../../../../../node_modules/firebase';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn: Observable<any>;//: Observable<User>;

  constructor(
    private nav: NavController,
    private auth: AngularFireAuth,
    private toast: ToastController
  ) {
    this.isLoggedIn = this.auth.authState;
   }

  
  login(user){

    this.auth.signInWithEmailAndPassword(user.email, user.password).
    then(() => this.nav.navigateForward('home')).
    catch(() => this.showError());
  }

  
  private async showError(){
    const ctrl = await this.toast.create({
      message: 'Dados de Acesso Incorretos!',
      duration: 3000
    });
    
    ctrl.present();

  }

  recoverPass(data){
    this.auth.sendPasswordResetEmail(data.email).
    then(() => this.nav.navigateBack('auth')).
    catch(err => {
      console.log(err);
    });

  }

  createUser(user){

    this.auth.createUserWithEmailAndPassword(user.email, user.password).
    then(credentials => console.log(credentials));

  }

  logout(){
    this.auth.signOut().
    then(() => this.nav.navigateBack('auth'));
  }

}
