// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDSmmEuq6D1JG8G6k_rk4FMGDIOEDnkbsA',
    authDomain: 'controle-if-craad.firebaseapp.com',
    databaseURL: 'https://controle-if-craad.firebaseio.com',
    projectId: 'controle-if-craad',
    storageBucket: 'controle-if-craad.appspot.com',
    messagingSenderId: '242599409353',
    appId: '1:242599409353:web:e362e0484f159602013f18',
    measurementId: 'G-78DSRRR8MR'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
