import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPage } from './login/login.page';
import { ForgotPage } from './forgot/forgot.page';
import { RegisterPage } from './register/register.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TopoLoginComponent } from './component/topo-login/topo-login.component';

const routes: Routes = [
  {
    path: '', children: [
      {path: '', component: LoginPage},
      {path: 'forgot', component: ForgotPage},
      {path: 'register', component: RegisterPage},
    ]
  }
];

@NgModule({
  imports: [CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)],
  declarations: [
    LoginPage,
    ForgotPage,
    RegisterPage,
    TopoLoginComponent
  ]
})
export class AuthRoutingModule { }
